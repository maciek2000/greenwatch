package pl.sulinski.greenwatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class GreenWatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreenWatchApplication.class, args);
    }

}
