package pl.sulinski.greenwatch.common.exception;

import org.springframework.security.core.AuthenticationException;

public class InvalidPasswordException extends AuthenticationException {

    public InvalidPasswordException(final String message, final Object... args) {
        super(String.format(message, args));
    }
}
