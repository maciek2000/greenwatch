package pl.sulinski.greenwatch.common.exception;

public class NotUniqueException extends RuntimeException {

    public NotUniqueException(final String message, final Object... args) {
        super(String.format(message, args));
    }
}
