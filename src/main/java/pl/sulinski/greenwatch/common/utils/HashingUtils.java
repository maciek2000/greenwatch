package pl.sulinski.greenwatch.common.utils;

import lombok.experimental.UtilityClass;

import static org.mindrot.jbcrypt.BCrypt.checkpw;
import static org.mindrot.jbcrypt.BCrypt.gensalt;
import static org.mindrot.jbcrypt.BCrypt.hashpw;

@UtilityClass
public class HashingUtils {

    public String hashPassword(final String password) {
        return hashpw(password, gensalt());
    }

    public boolean checkPassword(final String password, final String encryptedPassword) {
        return checkpw(password, encryptedPassword);
    }
}
