package pl.sulinski.greenwatch.common.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import static java.lang.Character.isLetterOrDigit;
import static java.lang.Character.isUpperCase;
import static java.lang.Character.isWhitespace;

public class PasswordValidator implements ConstraintValidator<StrongPassword, String> {

    public static final int REQUIRED_QUANTITY = 2;

    @Override
    public boolean isValid(final String password, final ConstraintValidatorContext context) {
        int specialChartsCounter = 0;
        int capitalLettersCounter = 0;
        for (final char letter : password.toCharArray()) {
            if (!isLetterOrDigit(letter) && !isWhitespace(letter)) {
                specialChartsCounter++;
            } else if (isUpperCase(letter)) {
                capitalLettersCounter++;
            }
        }
        return specialChartsCounter >= REQUIRED_QUANTITY && capitalLettersCounter >= REQUIRED_QUANTITY && !password.isBlank();
    }
}
