package pl.sulinski.greenwatch.connector;

import pl.sulinski.greenwatch.waqi.dto.IaqiDto;

import java.math.BigDecimal;

public interface WaqiDataFacade {

    IaqiDto getWaqiData(final BigDecimal latitude, final BigDecimal longitude);
}
