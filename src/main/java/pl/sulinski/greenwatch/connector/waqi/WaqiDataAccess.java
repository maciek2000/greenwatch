package pl.sulinski.greenwatch.connector.waqi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.sulinski.greenwatch.waqi.dto.IaqiDto;
import pl.sulinski.greenwatch.waqi.dto.WaqiResponseDto;

import java.math.BigDecimal;

@Component
class WaqiDataAccess {

    @Value("${waqi.api.token}")
    private String waqiApiToken;

    @Value("${waqi.api.url.template}")
    private String waqiApiUrlTemplate;

    public IaqiDto fetchData(final BigDecimal latitude, final BigDecimal longitude) {
        final String completelyUrl = String.format(waqiApiUrlTemplate, latitude, longitude, waqiApiToken);
        return new RestTemplate().getForObject(completelyUrl, WaqiResponseDto.class).getData().getIaqi();
    }
}
