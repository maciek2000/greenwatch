package pl.sulinski.greenwatch.connector.waqi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.sulinski.greenwatch.connector.WaqiDataFacade;

@Configuration
class WaqiDataFacadeConfiguration {

    @Bean
    WaqiDataFacade waqiDataFacade(final WaqiDataAccess waqiDataAccess) {
        return new WaqiDataServiceImpl(waqiDataAccess);
    }
}
