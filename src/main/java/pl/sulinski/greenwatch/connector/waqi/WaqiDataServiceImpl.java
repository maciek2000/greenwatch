package pl.sulinski.greenwatch.connector.waqi;

import lombok.RequiredArgsConstructor;
import pl.sulinski.greenwatch.connector.WaqiDataFacade;
import pl.sulinski.greenwatch.waqi.dto.IaqiDto;

import java.math.BigDecimal;

@RequiredArgsConstructor
class WaqiDataServiceImpl implements WaqiDataFacade {

    private final WaqiDataAccess waqiDataAccess;

    @Override
    public IaqiDto getWaqiData(final BigDecimal latitude, final BigDecimal longitude) {
        return waqiDataAccess.fetchData(latitude, longitude);
    }
}
