package pl.sulinski.greenwatch.job;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.sulinski.greenwatch.connector.WaqiDataFacade;
import pl.sulinski.greenwatch.localisation.LocalisationFacade;
import pl.sulinski.greenwatch.localisation.dto.LocalisationDto;
import pl.sulinski.greenwatch.waqi.WaqiFacade;
import pl.sulinski.greenwatch.waqi.dto.IaqiDto;
import pl.sulinski.greenwatch.waqi.dto.SaveAirQualityDataForm;

@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnProperty(value = "waqi.job.enabled", havingValue = "true")
class ReceiveWaqiDataJob {

    private final WaqiDataFacade waqiDataFacade;
    private final WaqiFacade waqiFacade;
    private final LocalisationFacade localisationFacade;

    private static final class LogMessages {
        static final String START_WAQI_JOB = "Start receiving waqi data job ...";
        static final String END_WAQI_JOB = "End receiving waqi data job";
    }

    @Scheduled(cron = "0 * * * * *")
    public void getWaqiData() {
        log.info(LogMessages.START_WAQI_JOB);

        localisationFacade.findAllLocalisations().forEach(localisationDto -> {
            IaqiDto iaqiDto = waqiDataFacade.getWaqiData(localisationDto.latitude(), localisationDto.longitude());
            waqiFacade.saveAirQualityData(createSaveAirQualityDataForm(localisationDto, iaqiDto));
        });

        log.info(LogMessages.END_WAQI_JOB);
    }

    private SaveAirQualityDataForm createSaveAirQualityDataForm(final LocalisationDto localisationDto,
                                                                final IaqiDto iaqiDto) {
        return new SaveAirQualityDataForm(iaqiDto, localisationDto.userId(), localisationDto.id());
    }
}
