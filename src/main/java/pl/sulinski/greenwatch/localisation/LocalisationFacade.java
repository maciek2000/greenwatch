package pl.sulinski.greenwatch.localisation;

import pl.sulinski.greenwatch.localisation.domain.LocalisationModel;
import pl.sulinski.greenwatch.localisation.dto.LocalisationDto;
import pl.sulinski.greenwatch.localisation.dto.SaveLocalisationForm;

import java.util.List;

public interface LocalisationFacade {
    void addLocalisation(final SaveLocalisationForm saveLocalisationForm);

    LocalisationModel findLocalisation(final Long localisationId);

    List<LocalisationDto> findAllLocalisations();
}
