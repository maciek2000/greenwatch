package pl.sulinski.greenwatch.localisation.controller;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.sulinski.greenwatch.connector.WaqiDataFacade;
import pl.sulinski.greenwatch.localisation.LocalisationFacade;
import pl.sulinski.greenwatch.localisation.dto.SaveLocalisationForm;

@RestController
@AllArgsConstructor
@RequestMapping("/api/localisation")
public class LocalisationController {


    private final LocalisationFacade localisationFacade;
    private final WaqiDataFacade waqiDataFacade;

    @PutMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public void addLocalisation(final @Valid @RequestBody SaveLocalisationForm saveLocalisationForm) {
        localisationFacade.addLocalisation(saveLocalisationForm);
    }
}
