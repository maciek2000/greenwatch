package pl.sulinski.greenwatch.localisation.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.sulinski.greenwatch.localisation.LocalisationFacade;
import pl.sulinski.greenwatch.user.UserFacade;

@Configuration
class LocalisationFacadeConfiguration {

    @Bean
    LocalisationFacade localisationFacade(final UserFacade userFacade, final LocalisationRepository localisationRepository) {
        return new LocalisationServiceImpl(userFacade, localisationRepository);
    }
}
