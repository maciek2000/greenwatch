package pl.sulinski.greenwatch.localisation.domain;

import lombok.experimental.UtilityClass;
import pl.sulinski.greenwatch.localisation.dto.LocalisationDto;
import pl.sulinski.greenwatch.localisation.dto.SaveLocalisationForm;
import pl.sulinski.greenwatch.user.domain.UserModel;

@UtilityClass
public class LocalisationMapper {

    LocalisationModel toLocalisationModel(final SaveLocalisationForm saveLocalisationForm, final UserModel user) {
        return LocalisationModel.builder()
                .longitude(saveLocalisationForm.longitude())
                .latitude(saveLocalisationForm.latitude())
                .user(user)
                .comment(saveLocalisationForm.comment())
                .build();
    }

    LocalisationDto toLocalisationDto(final LocalisationModel localisationModel) {
        return new LocalisationDto(
                localisationModel.getLatitude(),
                localisationModel.getLongitude(),
                localisationModel.getUser().getId(),
                localisationModel.getId());
    }
}
