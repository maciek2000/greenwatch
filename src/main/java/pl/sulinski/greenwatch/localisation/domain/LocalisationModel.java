package pl.sulinski.greenwatch.localisation.domain;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import pl.sulinski.greenwatch.common.entity.AbstractUUIDEntity;
import pl.sulinski.greenwatch.user.domain.UserModel;
import pl.sulinski.greenwatch.waqi.domain.WaqiModel;

import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "localisation")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class LocalisationModel extends AbstractUUIDEntity {

    @Column(name = "longitude", nullable = false)
    private BigDecimal longitude;

    @Column(name = "latitude", nullable = false)
    private BigDecimal latitude;

    @Column(name = "comment", nullable = false)
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private UserModel user;

    @OneToMany(mappedBy = WaqiModel.Fields.localisation,
            orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<WaqiModel> airQualityIndexes;
}
