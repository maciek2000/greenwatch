package pl.sulinski.greenwatch.localisation.domain;

import org.springframework.data.jpa.repository.JpaRepository;

interface LocalisationRepository extends JpaRepository<LocalisationModel, Long> {
}
