package pl.sulinski.greenwatch.localisation.domain;

import lombok.RequiredArgsConstructor;
import pl.sulinski.greenwatch.common.exception.NotFoundException;
import pl.sulinski.greenwatch.localisation.LocalisationFacade;
import pl.sulinski.greenwatch.localisation.dto.LocalisationDto;
import pl.sulinski.greenwatch.localisation.dto.SaveLocalisationForm;
import pl.sulinski.greenwatch.user.UserFacade;
import pl.sulinski.greenwatch.user.domain.UserModel;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor
class LocalisationServiceImpl implements LocalisationFacade {

    private final UserFacade userFacade;
    private final LocalisationRepository localisationRepository;

    @Override
    public void addLocalisation(final SaveLocalisationForm saveLocalisationForm) {
        UserModel user = userFacade.findByUserId(saveLocalisationForm.userId());
        localisationRepository.save(LocalisationMapper.toLocalisationModel(saveLocalisationForm, user));
    }

    @Override
    public LocalisationModel findLocalisation(final Long localisationId) {
        return localisationRepository.findById(localisationId)
                .orElseThrow(() -> new NotFoundException("Localisation with id: %s not found", localisationId));
    }

    @Override
    public List<LocalisationDto> findAllLocalisations() {
        return localisationRepository.findAll().stream()
                .map(LocalisationMapper::toLocalisationDto)
                .collect(toList());
    }
}
