package pl.sulinski.greenwatch.localisation.dto;

import java.math.BigDecimal;

public record LocalisationDto(BigDecimal latitude, BigDecimal longitude, Long userId, Long id) {
}
