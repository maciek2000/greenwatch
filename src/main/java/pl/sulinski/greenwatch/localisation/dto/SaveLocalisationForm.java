package pl.sulinski.greenwatch.localisation.dto;

import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

public record SaveLocalisationForm(@NotNull(message = "User id cannot be blank") Long userId,
                                   @NotNull(message = "Longitude cannot be null") BigDecimal longitude,
                                   @NotNull(message = "Latitude cannot be null") BigDecimal latitude,
                                   String comment) {
}
