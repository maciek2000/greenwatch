package pl.sulinski.greenwatch.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import pl.sulinski.greenwatch.user.UserFacade;
import pl.sulinski.greenwatch.user.dto.UserAuthenticationDto;
import pl.sulinski.greenwatch.user.dto.UserType;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
class ApiAuthenticationProvider implements AuthenticationProvider {

    private final UserFacade userFacade;
    private static final String ADMIN_API_ACCESS = "ADMIN";
    private static final String PREMIUM_USER_API_ACCESS = "PREMIUM_USER";
    private static final String REGULAR_USER_API_ACCESS = "REGULAR_USER";

    private static final class LogMessages {
        static final String START_AUTHENTICATION = "API: Start authentication user with login {}";
        static final String AUTHENTICATION_PASSED_ADMIN = "API: User with login {} authenticated successfully - ADMIN PERMISSIONS";
        static final String AUTHENTICATION_PASSED_PREMIUM_USER = "API: User with login {} authenticated successfully - PREMIUM USER PERMISSIONS";
        static final String AUTHENTICATION_PASSED_REGULAR_USER = "API: User with login {} authenticated successfully - REGULAR USER PERMISSIONS";
    }

    //ApiAuthenticationProvider will be improved when app will have more functionalities in the future
    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
        final String login = authentication.getName();
        final String password = authentication.getCredentials().toString();
        log.info(LogMessages.START_AUTHENTICATION, login);
        final UserAuthenticationDto authenticatedUser = userFacade.getAuthenticatedUser(login, password);
        if (UserType.ADMIN.equals(authenticatedUser.userType())) {
            log.info(LogMessages.AUTHENTICATION_PASSED_ADMIN, login);
            return new UsernamePasswordAuthenticationToken(login, password, List.of(new SimpleGrantedAuthority(ADMIN_API_ACCESS)));
        } else if (UserType.PREMIUM_USER.equals(authenticatedUser.userType())) {
            log.info(LogMessages.AUTHENTICATION_PASSED_PREMIUM_USER, login);
            return new UsernamePasswordAuthenticationToken(login, password, List.of(new SimpleGrantedAuthority(PREMIUM_USER_API_ACCESS)));
        } else {
            log.info(LogMessages.AUTHENTICATION_PASSED_REGULAR_USER, login);
            return new UsernamePasswordAuthenticationToken(login, password, List.of(new SimpleGrantedAuthority(REGULAR_USER_API_ACCESS)));
        }
    }

    @Override
    public boolean supports(final Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
