package pl.sulinski.greenwatch.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
class ApiSecurityConfiguration {

    private final ApiAuthenticationProvider authProvider;

    @Bean
    public SecurityFilterChain filterChain(final HttpSecurity http) throws Exception {
        return http
                .csrf().disable()
                .authorizeHttpRequests()
                .requestMatchers("/api**").hasAuthority("ADMIN")
                .requestMatchers("/api/localisation/create**").hasAnyAuthority("PREMIUM_USER", "REGULAR_USER")
                .requestMatchers("/api/account/create**").permitAll()
                .anyRequest().permitAll()
                .and()
                .authenticationProvider(authProvider)
                .httpBasic()
                .authenticationEntryPoint(new CustomAuthenticationEntryPoint())
                .and()
                .build();
    }
}
