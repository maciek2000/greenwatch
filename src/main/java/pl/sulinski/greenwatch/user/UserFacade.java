package pl.sulinski.greenwatch.user;

import pl.sulinski.greenwatch.user.domain.UserModel;
import pl.sulinski.greenwatch.user.dto.SaveUserForm;
import pl.sulinski.greenwatch.user.dto.UserAuthenticationDto;
import pl.sulinski.greenwatch.user.dto.UserType;

public interface UserFacade {
    void createAccount(final SaveUserForm saveUserForm);

    UserAuthenticationDto getAuthenticatedUser(final String login, final String password);

    void changeUserType(final Long id, final UserType userType);

    UserModel findByUserId(final Long userId);
}
