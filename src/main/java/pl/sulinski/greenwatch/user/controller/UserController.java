package pl.sulinski.greenwatch.user.controller;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.sulinski.greenwatch.user.UserFacade;
import pl.sulinski.greenwatch.user.dto.SaveUserForm;
import pl.sulinski.greenwatch.user.dto.UserType;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/account")
public class UserController {

    private final UserFacade userFacade;

    @PutMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public void createUserAccount(final @Valid @RequestBody SaveUserForm saveUserForm) {
        userFacade.createAccount(saveUserForm);
    }

    @PostMapping("/user_type")
    public void changeUserType(@NotNull @RequestParam("userId") final Long userId,
                               @NotNull @RequestParam("userType") final UserType userType) {
        userFacade.changeUserType(userId, userType);
    }
}
