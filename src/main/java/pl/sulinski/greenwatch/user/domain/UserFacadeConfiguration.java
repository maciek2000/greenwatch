package pl.sulinski.greenwatch.user.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.sulinski.greenwatch.user.UserFacade;

@Configuration
class UserFacadeConfiguration {

    @Bean
    UserFacade userFacade(final UserRepository userRepository) {
        return new UserServiceImpl(userRepository);
    }
}
