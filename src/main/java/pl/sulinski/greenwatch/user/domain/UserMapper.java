package pl.sulinski.greenwatch.user.domain;

import lombok.experimental.UtilityClass;
import pl.sulinski.greenwatch.common.utils.HashingUtils;
import pl.sulinski.greenwatch.user.dto.SaveUserForm;
import pl.sulinski.greenwatch.user.dto.UserAuthenticationDto;
import pl.sulinski.greenwatch.user.dto.UserType;

@UtilityClass
class UserMapper {

    UserModel toModel(final SaveUserForm saveUserForm) {
        return UserModel.builder()
                .login(saveUserForm.login())
                .password(HashingUtils.hashPassword(saveUserForm.password()))
                .email(saveUserForm.email())
                .phoneNumber(saveUserForm.phoneNumber())
                .userType(UserType.REGULAR_USER)
                .build();
    }

    UserAuthenticationDto toUserAuthenticationDto(final UserModel userModel) {
        return UserAuthenticationDto.builder()
                .login(userModel.getLogin())
                .userType(userModel.getUserType())
                .build();
    }
}
