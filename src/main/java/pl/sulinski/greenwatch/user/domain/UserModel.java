package pl.sulinski.greenwatch.user.domain;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.Check;
import pl.sulinski.greenwatch.common.entity.AbstractUUIDEntity;
import pl.sulinski.greenwatch.localisation.domain.LocalisationModel;
import pl.sulinski.greenwatch.user.dto.UserType;
import pl.sulinski.greenwatch.waqi.domain.WaqiModel;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "\"user\"")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class UserModel extends AbstractUUIDEntity {

    @Column(name = "login", nullable = false)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

    @Column(name = "user_type", nullable = false)
    @Enumerated(EnumType.STRING)
    @Check(constraints = "user_type IN ('REGULAR_USER', 'PREMIUM_USER', 'ADMIN')")
    private UserType userType;

    @Column(name = "created", nullable = false)
    private final LocalDateTime created = LocalDateTime.now(Clock.systemUTC());

    @OneToMany(mappedBy = LocalisationModel.Fields.user, cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.LAZY)
    private List<LocalisationModel> localisations;

    @OneToMany(mappedBy = WaqiModel.Fields.user, cascade = CascadeType.ALL,
            orphanRemoval = true, fetch = FetchType.LAZY)
    private List<WaqiModel> airQualityIndexes;
}