package pl.sulinski.greenwatch.user.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
interface UserRepository extends JpaRepository<UserModel, Long> {

    boolean existsByLogin(final String login);

    Optional<UserModel> findByLogin(final String login);
}
