package pl.sulinski.greenwatch.user.domain;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import pl.sulinski.greenwatch.common.exception.InvalidPasswordException;
import pl.sulinski.greenwatch.common.exception.NotFoundException;
import pl.sulinski.greenwatch.common.exception.NotUniqueException;
import pl.sulinski.greenwatch.common.utils.HashingUtils;
import pl.sulinski.greenwatch.user.UserFacade;
import pl.sulinski.greenwatch.user.dto.SaveUserForm;
import pl.sulinski.greenwatch.user.dto.UserAuthenticationDto;
import pl.sulinski.greenwatch.user.dto.UserType;

@RequiredArgsConstructor
class UserServiceImpl implements UserFacade {

    private final UserRepository userRepository;

    @Override
    public void createAccount(final SaveUserForm saveUserForm) {
        if (userRepository.existsByLogin(saveUserForm.login())) {
            throw new NotUniqueException("Provided login: %s is not unique", saveUserForm.login());
        }
        userRepository.save(UserMapper.toModel(saveUserForm));
    }

    @Override
    public UserAuthenticationDto getAuthenticatedUser(final String login, final String password) {
        return userRepository.findByLogin(login)
                .map(userModel -> {
                    final boolean isPasswordCorrect = HashingUtils.checkPassword(password, userModel.getPassword());
                    if (isPasswordCorrect) {
                        return UserMapper.toUserAuthenticationDto(userModel);
                    }
                    throw new InvalidPasswordException("Provided password is invalid for user with login: %s", login);
                })
                .orElseThrow(() -> new NotFoundException("User with login: %s not found", login));
    }

    @Override
    @Transactional
    public void changeUserType(final Long id, final UserType userType) {
        final UserModel userModel = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User with id: %s not found", id));
        userModel.setUserType(userType);
    }

    @Override
    public UserModel findByUserId(final Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("User with id: %s not found", userId));
    }
}
