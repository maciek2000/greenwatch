package pl.sulinski.greenwatch.user.dto;

import jakarta.validation.constraints.NotBlank;
import pl.sulinski.greenwatch.common.validation.StrongPassword;

public record SaveUserForm(@NotBlank(message = "login cannot be blank") String login,
                           @StrongPassword String password,
                           @NotBlank(message = "email cannot be blank") String email,
                           @NotBlank(message = "phone number cannot be blank") String phoneNumber) {
}
