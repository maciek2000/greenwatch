package pl.sulinski.greenwatch.user.dto;

import lombok.Builder;

@Builder
public record UserAuthenticationDto(String login, UserType userType) {
}
