package pl.sulinski.greenwatch.user.dto;

public enum UserType {
    REGULAR_USER,
    PREMIUM_USER,
    ADMIN
}
