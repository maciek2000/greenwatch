package pl.sulinski.greenwatch.waqi;

import pl.sulinski.greenwatch.waqi.dto.SaveAirQualityDataForm;

public interface WaqiFacade {

    void saveAirQualityData(final SaveAirQualityDataForm form);
}
