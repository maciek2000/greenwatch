package pl.sulinski.greenwatch.waqi.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.sulinski.greenwatch.localisation.LocalisationFacade;
import pl.sulinski.greenwatch.user.UserFacade;
import pl.sulinski.greenwatch.waqi.WaqiFacade;

@Configuration
class WaqiFacadeConfiguration {

    @Bean
    WaqiFacade waqiFacade(final WaqiRepository waqiRepository, final UserFacade userFacade, final LocalisationFacade localisationFacade) {
        return new WaqiServiceImpl(waqiRepository, userFacade, localisationFacade);
    }
}
