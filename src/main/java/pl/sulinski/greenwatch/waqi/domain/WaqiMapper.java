package pl.sulinski.greenwatch.waqi.domain;

import lombok.experimental.UtilityClass;
import pl.sulinski.greenwatch.localisation.domain.LocalisationModel;
import pl.sulinski.greenwatch.user.domain.UserModel;
import pl.sulinski.greenwatch.waqi.dto.IaqiDto;
import pl.sulinski.greenwatch.waqi.dto.IaqiElementDto;

import java.math.BigDecimal;

import static java.util.Objects.isNull;

@UtilityClass
class WaqiMapper {

    WaqiModel toWaqiModel(final IaqiDto iaqi, final UserModel user, final LocalisationModel localisation) {
        return WaqiModel.builder()
                .dew(getV(iaqi.getDew()))
                .h(getV(iaqi.getH()))
                .no2(getV(iaqi.getNo2()))
                .o3(getV(iaqi.getO3()))
                .p(getV(iaqi.getP()))
                .pm10(getV(iaqi.getPm10()))
                .r(getV(iaqi.getR()))
                .t(getV(iaqi.getT()))
                .w(getV(iaqi.getW()))
                .user(user)
                .localisation(localisation)
                .build();
    }

    private BigDecimal getV(IaqiElementDto iaqiElementDto) {
        return isNull(iaqiElementDto) ? null : iaqiElementDto.getV();
    }
}
