package pl.sulinski.greenwatch.waqi.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import pl.sulinski.greenwatch.common.entity.AbstractUUIDEntity;
import pl.sulinski.greenwatch.localisation.domain.LocalisationModel;
import pl.sulinski.greenwatch.user.domain.UserModel;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;

@Entity
@Table(name = "waqi")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
public class WaqiModel extends AbstractUUIDEntity {

    //WAQI - World Air Quality Index
    @Column(name = "dew")
    private BigDecimal dew;

    @Column(name = "h")
    private BigDecimal h;

    @Column(name = "no2")
    private BigDecimal no2;

    @Column(name = "o3")
    private BigDecimal o3;

    @Column(name = "p")
    private BigDecimal p;

    @Column(name = "pm10")
    private BigDecimal pm10;

    @Column(name = "r")
    private BigDecimal r;

    @Column(name = "t")
    private BigDecimal t;

    @Column(name = "w")
    private BigDecimal w;

    @Column(name = "time", nullable = false)
    private final LocalDateTime time = LocalDateTime.now(Clock.systemUTC());

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private UserModel user;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "localisation_id", nullable = false)
    private LocalisationModel localisation;
}
