package pl.sulinski.greenwatch.waqi.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface WaqiRepository extends JpaRepository<WaqiModel, Long> {

}
