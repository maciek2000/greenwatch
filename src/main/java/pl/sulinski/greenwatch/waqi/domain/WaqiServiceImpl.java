package pl.sulinski.greenwatch.waqi.domain;

import lombok.RequiredArgsConstructor;
import pl.sulinski.greenwatch.localisation.LocalisationFacade;
import pl.sulinski.greenwatch.localisation.domain.LocalisationModel;
import pl.sulinski.greenwatch.user.UserFacade;
import pl.sulinski.greenwatch.user.domain.UserModel;
import pl.sulinski.greenwatch.waqi.WaqiFacade;
import pl.sulinski.greenwatch.waqi.dto.SaveAirQualityDataForm;

@RequiredArgsConstructor
class WaqiServiceImpl implements WaqiFacade {

    private final WaqiRepository waqiRepository;
    private final UserFacade userFacade;
    private final LocalisationFacade localisationFacade;

    @Override
    public void saveAirQualityData(final SaveAirQualityDataForm form) {
        UserModel user = userFacade.findByUserId(form.userId());
        LocalisationModel localisation = localisationFacade.findLocalisation(form.localisationId());
        waqiRepository.save(WaqiMapper.toWaqiModel(form.iaqiDto(), user, localisation));
    }
}
