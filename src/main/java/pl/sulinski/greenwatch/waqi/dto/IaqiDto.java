package pl.sulinski.greenwatch.waqi.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IaqiDto {

    private IaqiElementDto dew;
    private IaqiElementDto h;
    private IaqiElementDto no2;
    private IaqiElementDto o3;
    private IaqiElementDto p;
    private IaqiElementDto pm10;
    private IaqiElementDto r;
    private IaqiElementDto t;
    private IaqiElementDto w;
}