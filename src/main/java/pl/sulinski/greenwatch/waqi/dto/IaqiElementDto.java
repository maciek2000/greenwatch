package pl.sulinski.greenwatch.waqi.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class IaqiElementDto {

    private BigDecimal v;
}