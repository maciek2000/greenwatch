package pl.sulinski.greenwatch.waqi.dto;

public record SaveAirQualityDataForm(IaqiDto iaqiDto, Long userId, Long localisationId) {
}
