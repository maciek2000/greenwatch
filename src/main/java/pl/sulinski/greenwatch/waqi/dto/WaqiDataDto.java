package pl.sulinski.greenwatch.waqi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class WaqiDataDto {

    private int aqi;
    private int idx;
    private IaqiDto iaqi;
}