package pl.sulinski.greenwatch.waqi.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WaqiResponseDto {

    //WAQI - World Air Quality Index
    private String status;
    private WaqiDataDto data;
}