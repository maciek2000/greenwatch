--liquibase formatted sql
--changeset maciej.sulinski:1

CREATE SEQUENCE hibernate_sequence;

CREATE TABLE "user"
(
    id           BIGINT PRIMARY KEY,
    uuid         UUID UNIQUE    NOT NULL,
    login        VARCHAR UNIQUE NOT NULL,
    password     VARCHAR        NOT NULL,
    email        VARCHAR        NOT NULL,
    phone_number VARCHAR        NOT NULL,
    user_type    VARCHAR        NOT NULL CHECK (user_type IN ('REGULAR_USER', 'PREMIUM_USER', 'ADMIN')),
    created      TIMESTAMP      NOT NULL DEFAULT NOW()
)

--rollback DROP TABLE "user";
--rollback DROP SEQUENCE hibernate_sequence;