--liquibase formatted sql
--changeset maciej.sulinski:2

CREATE TABLE "localisation"
(
    id        BIGINT PRIMARY KEY,
    uuid      UUID UNIQUE NOT NULL,
    longitude NUMERIC     NOT NULL,
    latitude  NUMERIC     NOT NULL,
    comment   VARCHAR,
    user_id   BIGINT      NOT NULL
        CONSTRAINT fk_localisation_user_id REFERENCES "user" (id) ON DELETE CASCADE
)

--rollback DROP TABLE "localisation";