--liquibase formatted sql
--changeset maciej.sulinski:3

CREATE TABLE "waqi"
(
    id              BIGINT PRIMARY KEY,
    uuid            UUID UNIQUE NOT NULL,
    dew             NUMERIC,
    h               NUMERIC,
    no2             NUMERIC,
    o3              NUMERIC,
    p               NUMERIC,
    pm10            NUMERIC,
    r               NUMERIC,
    t               NUMERIC,
    w               NUMERIC,
    localisation_id BIGINT      NOT NULL
        CONSTRAINT fk_waqi_localisation_id REFERENCES "localisation" (id) ON DELETE CASCADE,
    user_id         BIGINT      NOT NULL
        CONSTRAINT fk_waqi_user_id REFERENCES "user" (id) ON DELETE CASCADE
)

--rollback DROP TABLE "waqi";