--liquibase formatted sql
--changeset maciej.sulinski:3

ALTER TABLE waqi ADD COLUMN time TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW();

--rollback ALTER TABLE waqi DROP COLUMN time;